﻿using LVL2_ASPNet_MVC_04.Models;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace LVL2_ASPNet_MVC_04
{
    public class MyAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            db_costumerEntities db = new db_costumerEntities();
            var u = db.tbl_user.Where(x => x.Pk_User_id == 1).FirstOrDefault();
            if (context.UserName == u.username.Trim() && context.Password == u.password.Trim())
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, "admin"));
                identity.AddClaim(new Claim("username", "admin"));
                identity.AddClaim(new Claim(ClaimTypes.Name, "M Agung Gumelar"));
                context.Validated(identity);
            }
            else if (context.UserName == "user" && context.Password == "user")
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, "user"));
                identity.AddClaim(new Claim("username", "user"));
                identity.AddClaim(new Claim(ClaimTypes.Name, "Imam"));
                context.Validated(identity);
            }
            else
            {
                context.SetError("Invalid Grant", "Provide username and password is incorrect");
                return;
            }
        }
    }
}