﻿using LVL2_ASPNet_MVC_04.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace LVL2_ASPNet_MVC_04.Controllers
{
    public class DataController : ApiController
    {
        [AllowAnonymous]
        [HttpGet]
        [Route("api/data/forall")]

        public IEnumerable<tbl_costumer> Get()
//        public IHttpActionResult Get()
        {
            db_costumerEntities db = new db_costumerEntities();
            return db.tbl_costumer.ToList();
//            return Ok("Now server time is : " + DateTime.Now.ToString());
        }

        [Authorize]
        [HttpGet]
        [Route("api/data/authenticate")] // Buat Akses USer
        public IHttpActionResult GetForAuthenticate()
        {
            var identity = (ClaimsIdentity)User.Identity;
            return Ok("Hello " + identity.Name);
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        [Route("api/data/authorize")]
        public IEnumerable<tbl_user> GetForAdmin()
 //       public IHttpActionResult GetForAdmin()
        {
            db_costumerEntities db = new db_costumerEntities();
            var identity = (ClaimsIdentity)User.Identity;
            var roles = identity.Claims.Where(x => x.Type == ClaimTypes.Role).Select(x => x.Value);
            return db.tbl_user.ToList();
            // return Ok ("Hello " + identity.Name + " Role :" + string.Join(",", roles.ToList()));
        }
    }
}
